<?php
/**
 * @file
 * ns_entrpr.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ns_entrpr_taxonomy_default_vocabularies() {
  return array(
    'ns_entrpr_article_topic' => array(
      'name' => 'Article Topic',
      'machine_name' => 'ns_entrpr_article_topic',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'ns_entrpr_promo_region' => array(
      'name' => 'Promotion region',
      'machine_name' => 'ns_entrpr_promo_region',
      'description' => 'Regions for promtions',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'ns_entrpr_section' => array(
      'name' => 'Section',
      'machine_name' => 'ns_entrpr_section',
      'description' => 'A section is a main part of the site.',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
